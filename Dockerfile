FROM php:8.3-fpm
RUN apt-get update &&  apt-get install -y \ 
    libpng-dev \
    libjpeg-dev \
    libfreetype6-dev \
    libicu-dev \
    libzip-dev \
    unzip \
    git \
    curl \
    nginx \
    lsof
RUN docker-php-ext-configure gd --with-jpeg --with-freetype \
    && docker-php-ext-install gd pdo_mysql intl zip 
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN mkdir -p /run/nginx && chown -R www-data:www-data /run/nginx && chmod -R 775 /run/nginx
RUN mkdir -p /var/lib/nginx/body /var/lib/nginx/proxy /var/lib/nginx/fastcgi /var/lib/nginx/uwsgi /var/lib/nginx/scgi \
    && chown -R www-data:www-data /var/lib/nginx \
    && chmod -R 755 /var/lib/nginx
COPY nginx/nginx.conf /etc/nginx/nginx.conf
RUN rm /etc/nginx/sites-enabled/default
COPY nginx/devops-ecs.conf /etc/nginx/sites-enabled/default
WORKDIR /var/www/html
COPY . /var/www/html
RUN mkdir -p /home/www-data/.composer && \
    chown -R www-data:www-data /home/www-data /var/www/html /home/www-data/.composer
COPY entrypoint.sh /entrypoint.sh
RUN chown www-data:www-data /entrypoint.sh && chmod +x /entrypoint.sh
ENV HOME=/home/www-data
USER www-data
RUN composer config --global allow-plugins true
RUN composer install --no-dev --optimize-autoloader
EXPOSE 80 9000
CMD ["/entrypoint.sh"]

