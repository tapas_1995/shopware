#!/bin/bash
# Stop any existing PHP-FPM processes to avoid port conflicts
pkill php-fpm || true

# Start PHP-FPM
/usr/local/sbin/php-fpm

# Start nginx in foreground
nginx -g 'daemon off;
